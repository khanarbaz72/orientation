**GIT**
- Git is a distributed version-control system for tracking changes in source code during software development. 
- It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.
- Its goals include speed, data integrity, and support for distributed, non-linear workflows.
![git][https://gitlab.com/khanarbaz72/orientation/-/blob/master/git_images/git-1.png]

**Important terms related to git**
- **Repository** : It is often called as a repo. A repository is the collection of files and folders i.e. code files that you’re using git to track. It is type of box where you place your inside it .
![repository][https://gitlab.com/khanarbaz72/orientation/-/blob/master/git_images/repository.jpg]
- **GitLab** : GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc.
![gitlab][https://gitlab.com/khanarbaz72/orientation/-/blob/master/git_images/gitlab.png]
- **Commit** : It is used for saving the work .When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.
- **Push** : The push will move your work from local repository to remote repository.
- **Branch** : The trunk of the tree, the main software, is called the Master Branch. The branches of that tree are called branches. These are separate instances of the code that is different from the main codebase.
- **Merge** :Merging is Git's way of putting a forked history back together again. The git merge command lets you take the independent lines of development created by git branch and integrate them into a single branch. 
- **Clone** :Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.
- **Fork** :Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.
![clone][https://gitlab.com/khanarbaz72/orientation/-/blob/master/git_images/clone.jpg]

