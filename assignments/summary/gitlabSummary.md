**GitLab**
- GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain. 
- GitLab is a Git-based repository manager and a powerful complete application for software development.
- With an "user-friendly" interface, GitLab allows you to work effectively, both from the command line and from the UI itself.
![gitlab][https://gitlab.com/khanarbaz72/orientation/-/blob/master/git_images/gitlab.png]